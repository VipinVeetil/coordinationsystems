from __future__ import division
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import csv
import parameters as p

centralized = pd.read_csv('centralized_probability_of_error.csv',skiprows=[0], header=None)
centralized_values = centralized.iloc[:,1:]
decentralized = pd.read_csv('decentralized_probability_of_error.csv',skiprows=[0], header=None)
decentralized_values = decentralized.iloc[:,1:]
errors = list(centralized.iloc[:,0])



centralized_mean = []
decentralized_mean = []
for i in xrange(len(errors)):
    centralized_mean.append(np.mean(centralized_values.iloc[i,:]))
    decentralized_mean.append(np.mean(decentralized_values.iloc[i,:]))

plt.plot(errors, centralized_mean, color = "tomato", label = "Centralized System")
plt.plot(errors, decentralized_mean, color = "navy", label = "Decentralized System")
plt.title("Mean of Coordination for Different Probabilities of Error")
plt.xlabel("Probability of error")
plt.ylabel("Mean of Coordination")
plt.legend(loc = 1, fontsize = 10)
plt.savefig('mean.png', bbox_inches='tight')
plt.close()




centralized_var = []
decentralized_var = []
for i in xrange(len(errors)):
    centralized_var.append(np.var(centralized_values.iloc[i,:]))
    decentralized_var.append(np.var(decentralized_values.iloc[i,:]))

plt.plot(errors, centralized_var, color = "tomato", label = "Centralized System")
plt.plot(errors, decentralized_var, color = "navy", label = "Decentralized System")
plt.title("Variance of Coordination for Different Probabilities of Error")
plt.xlabel("Probability of error")
plt.ylabel("Variance of Coordination")
plt.legend(loc = 1, fontsize = 10)
plt.savefig('variance.png', bbox_inches='tight')
plt.close()



bins_value = 50
transparency = 0.5
fontsize_value = 10
loc_value = 1

plt.hist(list(centralized_values.iloc[5,:]), bins = bins_value, color = "tomato", alpha = transparency, label = "Centralized System", ec = "tomato")
plt.hist(list(decentralized_values.iloc[5,:]), bins = bins_value, color = "navy", alpha = transparency, label = "Decentralized System", ec = "navy")
plt.title("Histogram of Coordination with 0.05 Probability of Error")
plt.ylabel("Frequency")
plt.xlabel("Coordination")
plt.legend(loc = loc_value, fontsize = fontsize_value)
plt.savefig('hist5.png', bbox_inches='tight')
plt.close()


plt.hist(list(centralized_values.iloc[10,:]), bins = bins_value, color = "tomato", alpha = transparency, label = "Centralized System", ec = "tomato")
plt.hist(list(decentralized_values.iloc[10,:]), bins = bins_value, color = "navy", alpha = transparency, label = "Decentralized System", ec = "navy")
plt.title("Histogram of Coordination with 0.10 Probability of Error")
plt.ylabel("Frequency")
plt.xlabel("Coordination")
plt.legend(loc = loc_value, fontsize = fontsize_value)
plt.savefig('hist10.png', bbox_inches='tight')
plt.close()


plt.hist(list(centralized_values.iloc[20,:]), bins = bins_value, color = "tomato", alpha = transparency, label = "Centralized System", ec = "tomato")
plt.hist(list(decentralized_values.iloc[20,:]), bins = bins_value, color = "navy", alpha = transparency, label = "Decentralized System", ec = "navy")
plt.title("Histogram of Coordination with 0.20 Probability of Error")
plt.ylabel("Frequency")
plt.xlabel("Coordination")
plt.legend(loc = loc_value, fontsize = fontsize_value)
plt.savefig('hist20.png', bbox_inches='tight')
plt.close()


plt.hist(list(centralized_values.iloc[50,:]), bins = bins_value, color = "tomato", alpha = transparency, label = "Centralized System", ec = "tomato")
plt.hist(list(decentralized_values.iloc[50,:]), bins = bins_value, color = "navy", alpha = transparency, label = "Decentralized System", ec = "navy")
plt.title("Histogram of Coordination with 0.50 Probability of Error")
plt.ylabel("Frequency")
plt.xlabel("Coordination")
plt.legend(loc = loc_value, fontsize = fontsize_value)
plt.savefig('hist50.png', bbox_inches='tight')
plt.close()




centralized_data = []
decentralized_data = []
count = 0
errors_box = []
increment = 1
for i in xrange(int(len(errors)/increment)):
    decentralized_data.append(list(decentralized_values.iloc[count,:]))
    centralized_data.append(list(centralized_values.iloc[count,:]))
    errors_box.append(errors[count])
    count += increment
errors_box.append(1)
new_errors_box = []
for i in xrange(len(errors_box)):
    if i % 10 == 0:
        new_errors_box.append(errors_box[i])
    else:
        new_errors_box.append('')

def draw_plot(data, edge_color, fill_color):
    bp = ax.boxplot(data, patch_artist=True)
    
    for element in ['boxes', 'whiskers', 'fliers', 'medians', 'caps']:
        plt.setp(bp[element], color=edge_color)

    plt.xticks(xrange(int(100 / increment)+1), new_errors_box)

    plt.xlabel("Probability of error")
    plt.ylabel("Coordination")
    plt.title("Coordination for Different Probabilities of Error")

    

    for patch in bp['boxes']:
        patch.set(facecolor=fill_color)
    

fig, ax = plt.subplots()
draw_plot(centralized_data, "tomato", "white")
draw_plot(decentralized_data, "navy", "white")
plt.text(0.50, -35, "Centralized System", color = "tomato", fontsize = 12)
plt.text(0.50, -38, "Decentralized System", color = "navy", fontsize = 12)
plt.savefig('box.png', bbox_inches='tight')
plt.close()


decentralized_time_steps = pd.read_csv('decentralized_number_of_agents.csv',skiprows=[0], header=None)
decentralized_values_time_steps = decentralized_time_steps.iloc[:,1:]
csv_file_name = "decentralized_time_steps_mean"
with open('%s.csv' % csv_file_name, 'wb') as csvfile:
    for i in xrange(100):
        mean_agent = []
        for k in xrange(1500):
            mean_for_time_step = np.mean(list(decentralized_values_time_steps.iloc[count + 0: count + 999, k]))
            mean_agent.append(mean_for_time_step)
        writer = csv.writer(csvfile, delimiter = ',')
        agents = 10 * i
        writer.writerow([agents] + mean_agent)

centralized = pd.read_csv('centralized_number_of_agents.csv', skiprows=[0], header=None)
centralized_values = centralized.iloc[:,1:]

csv_file_name = "centralized_mean"
with open('%s.csv' % csv_file_name, 'wb') as csvfile:
    for i in xrange(100):
        writer = csv.writer(csvfile, delimiter = ',')
        mean_agent = np.mean(list(centralized_values.iloc[i]))
        agents = 10 * i + 10
        writer.writerow([agents] + [mean_agent])

decentralized_mean_time_steps = pd.read_csv('decentralized_time_steps_mean.csv', header=None)
decentralized_mean_time_steps_values = decentralized_mean_time_steps.iloc[:,1:]
centralized_mean = pd.read_csv('centralized_mean.csv', header=None)
centralized_mean = centralized_mean.iloc[:,1]
a = list(decentralized_mean_time_steps_values.iloc[0])

csv_file_name = "time_steps_to_exceed_centralized"

def return_time_step(number_of_agents):
    c_mean = centralized_mean[i]
    d_mean_list = list(decentralized_mean_time_steps_values.iloc[i])
    for j in xrange(1500):
        if d_mean_list[j] > c_mean:
            return j


with open('%s.csv' % csv_file_name, 'wb') as csvfile:
    for i in xrange(100):
        agents = 10 * i + 10
        writer = csv.writer(csvfile, delimiter = ',')
        writer.writerow([agents] + [return_time_step(i)])

time_steps = pd.read_csv('time_steps_to_exceed_centralized.csv', header=None)
time_steps_values = list(time_steps.iloc[:,1])
agents = list(time_steps.iloc[:,0])

plt.scatter(agents, time_steps_values, color = "tomato", marker = '.', s = 5)
plt.title("Time Necessary for Decentralized System to Exceed Coordination of Centralized System")
plt.xlabel("Number of agents")
plt.ylabel("Time steps")
plt.xlim([0,1050])
plt.ylim([0,1050])
plt.savefig('time_steps.png', bbox_inches='tight')





