# Coordination in Centralized and Decentralized Systems
# This file contains the System Class. System Class is the parent class of Class Decentralized System and Class Centralized System
# Language: Python

from __future__ import division
import random

import parameters as p
import agent

class System(object): # this class contains common features of centralized and decentralized systems
    def __init__(self):
        self.agents = [] # a list of agents
        self.coordination = 0
    
    def create_agents(self): # create instances of agent class
        self.agents = [agent.Agent() for i in range(p.number_of_agents)]

    def assign_attributes(self): # assign attributes to agents
        count = 0
        for agent in self.agents:
            agent.number = count
            count += 1
        random.shuffle(self.agents) # suffle the list of agents so that the agents are in random order
        for agent in self.agents: # assign each agent its initial position on the line
            agent.position = self.agents.index(agent)

    def compute_coordination(self): # compute level of coordination in the system
        position = 0
        for agent in self.agents:
            self.coordination -= abs(position - agent.number)
            position += 1
        self.coordination = self.coordination / p.number_of_agents

