# Coordination in Centralized and Decentralized Systems
# This file contains the Decentralized Systems Class.
# Language: Python

from __future__ import division
import copy
import random
from datetime import datetime
random.seed(datetime.now())

import parameters as p
import system

class Decentralized_System(system.System):
    def __init__(self):
        system.System.__init__(self) # decentralized_system inherits attributes of Class System
        self.coordination = 0 # coordination of decentralized system at end of process
        self.coordination_time_series = [] # a list of the coordination in the decentralized over time
        
        self.two_less_than_number_of_agents = p.number_of_agents - 2
        self.range_number_agents = xrange(p.number_of_agents)
        self.range_time_steps = xrange(p.decentralized_system_time_steps)
    
    def binary_interaction(self): # two adjacent agents interact
        a0_position = random.randint(0, self.two_less_than_number_of_agents) # position of randomly chosen agent a0
        a1_position = a0_position + 1 # position of adjacent agent a1
        a0 = self.agents[a0_position] # select agent a0 from list of agents
        a1 = self.agents[a1_position] # select agent a1 from list of agents
        a0_number = a0.return_number() # a0 returns its number
        a1_number = a1.return_number() # a1 returns its number
        if a0_number > a1_number: # is the number of a0 is greater than number of a1 the swap positions
            self.agents[a0_position], self.agents[a1_position] = self.agents[a1_position], self.agents[a0_position] # swap the position of the two agents

    def interactions(self): # agents interact
        for i in self.range_number_agents: # as many interactions as number of agents in the system
            self.binary_interaction()
    
    def compute_coordination(self):
        position = 0
        coordination = 0
        for agent in self.agents:
            coordination -= abs(position - agent.number) # coordination of an agent is absolute value of its position on the line minus the agent's number
            position += 1
        coordination = coordination / p.number_of_agents
        return coordination
        

    def process(self): # process of decentralized interactions
        for time in self.range_time_steps: # for each time step
            self.interactions() # agents interact
            if p.compute_decentralized_coordination_over_time == True: # compute the coordination of decentralized system at end of each time step is the parameter is True.
                self.coordination_time_series.append(self.compute_coordination())
        self.coordination = self.compute_coordination()

