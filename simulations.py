from __future__ import division

import csv
import numpy as np
from multiprocessing import Pool

import main
import parameters as p
import datetime
from time import time



def run(system_name_value):
    system = system_name_value[0]
    parameter_name = system_name_value[1]
    parameter_value = system_name_value[2]
    
    if parameter_name == "probability_of_error":
        p.probability_of_error =  parameter_value
    elif parameter_name == "number_of_agents":
        p.number_of_agents =  parameter_value
    
    if system == "centralized" and parameter_name == "probability_of_error":
        main_instance = main.Main()
        main_instance.iterations = p.iterations_error
        main_instance.centralized_iterations()
        return main_instance.centralized_coordination

    elif system == "centralized" and parameter_name == "number_of_agents":
        main_instance = main.Main()
        main_instance.iterations = p.iterations_agents
        main_instance.centralized_iterations()
        return main_instance.centralized_coordination


    elif system == "decentralized" and parameter_name == "probability_of_error":
        main_instance = main.Main()
        main_instance.iterations = p.iterations_error
        main_instance.decentralized_iterations()
        return main_instance.decentralized_coordination

    elif system == "decentralized" and parameter_name == "number_of_agents":
        main_instance = main.Main()
        main_instance.iterations = p.iterations_agents
        main_instance.decentralized_iterations()
        return main_instance.decentralized_coordination_time_steps


def write_data(system_name_values_data):
    system = system_name_values_data[0]
    parameter_name = system_name_values_data[1]
    range_parameter_values = system_name_values_data[2]
    all_data = system_name_values_data[3]
    
    csv_file_name = system + "_" + parameter_name
    number_of_parameter_values = len(range_parameter_values)
    
    with open('%s.csv' % csv_file_name, 'wb') as csvfile:
        if system == "decentralized" and parameter_name == "number_of_agents":
            writer = csv.writer(csvfile, delimiter = ',')
            writer.writerow([parameter_name] + ["coordination_values at each time step all columns except first"] + ["simulation year/month/day", datetime.date.today()])
            for i in xrange(number_of_parameter_values):
                iterations_for_parameter_value = all_data[i]
                for j in xrange(p.iterations_agents):
                    one_iteration = iterations_for_parameter_value[j]
                    writer = csv.writer(csvfile, delimiter = ',')
                    writer.writerow([range_parameter_values[i]] + one_iteration)
        else:
            writer = csv.writer(csvfile, delimiter = ',')
            writer.writerow([parameter_name] + ["coordination_values at end of process all columns except first"] + ["simulation year/month/day", datetime.date.today()])
            for i in xrange(number_of_parameter_values):
                writer = csv.writer(csvfile, delimiter = ',')
                writer.writerow([range_parameter_values[i]] + all_data[i])

                             
def set_generate_parameter_values(parameter_name):
    if parameter_name == "probability_of_error":
        increment_error = 0.01
        range_parameter_values = np.arange(0.0, 1.01, increment_error)

        p.number_of_agents = 100
        p.decentralized_system_time_steps = 1000
    
    elif parameter_name == "number_of_agents":
        increment_agents = 10
        range_parameter_values = range(10, 1001, increment_agents)
         
        p.probability_of_error = 0.1
        p.decentralized_system_time_steps = 1500
        p.compute_decentralized_coordination_over_time =  True

    return range_parameter_values
                             

def generate_data(system_name_values):
    system = system_name_values[0]
    parameter_name = system_name_values[1]
    range_parameter_values = system_name_values[2]
    

    number_of_parameter_values =  len(range_parameter_values)
    loops = int(number_of_parameter_values / p.cores)
    reminder = int(number_of_parameter_values % p.cores)
    if reminder != 0:
        max_loops = loops + 1
    all_data = []
    count = 0
    m = number_of_parameter_values - reminder
    for i in xrange(max_loops):
        if count <= m:
            pool = Pool(p.cores)
            parameter_values = range_parameter_values[count : (count + p.cores)]
            count += p.cores
        else:
            pool = Pool(reminder)
            parameter_values = range_parameter_values[count : (count + reminder)]


        print system, parameter_name, parameter_values
        system_name_value = []
        for value in parameter_values:
            system_name_value.append((system, parameter_name, value))

        data = pool.map(run, system_name_value)
        pool.close()
        pool.join()
        all_data = all_data + data

    return all_data
     
                             
def parameter_sweep(system, parameter_name): #if system code is "centralized" it runs centralized system, if "decentralized" it runs decentralized system
    
    range_parameter_values = set_generate_parameter_values(parameter_name)
    system_name_values = [system, parameter_name, range_parameter_values]
    all_data = generate_data(system_name_values)
    system_name_values_data = [system, parameter_name, range_parameter_values, all_data]
    write_data(system_name_values_data)
                             


parameter_sweep("centralized", "probability_of_error")
parameter_sweep("decentralized", "probability_of_error")
parameter_sweep("centralized", "number_of_agents")
parameter_sweep("decentralized", "number_of_agents")
