# Coordination in Centralized and Decentralized Systems
# This file contains the Agent Class. Agents populate the centralized and decentralized systems
# Language: Python

from __future__ import division
import random
from datetime import datetime
random.seed(datetime.now())
import parameters as p

class Agent(object): # agent class
    def __init__(self):
        self.number = 0 # agent's number
        self.all_numbers = xrange(p.number_of_agents) # a list of numbers from which the agent report a number when it makes an error in reporting
    
    def return_number(self):
        if p.probability_of_error < random.uniform(0,1): # if an agent does  not make an error, report its true number
            return self.number
        else: # if an agent makes an error, report a random number from list of possible numbers
            return random.choice(self.all_numbers)

