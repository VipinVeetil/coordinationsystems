# Coordination in Centralized and Decentralized Systems
# This file contains the Centralized Systems Class.
# Language: Python

from __future__ import division
import copy
import random
from datetime import datetime
random.seed(datetime.now())

from scipy.stats import mode
import numpy as np
import sys

import parameters as p
import system

sys.setrecursionlimit(25 * p.number_of_agents)


class Centralized_System(system.System):
    def __init__(self):
        system.System.__init__(self) # centralized system class inherits attributes of Class System
        self.ordered_agents = [0] * p.number_of_agents # a list of agents place in order by the centralized mechanism
        self.highest_position = (p.number_of_agents - 1)
        self.end_point_reached = None # used to search for empty positions
    
    def search_neighborhood(self, position, distance): # search for an empty place in the neighborhood of the "position" argument that is "distance" argument apart from "position" argument
        if position == 0:
            self.end_point_reached = "left"
        elif position == self.highest_position:
            self.end_point_reached = "right"
        
        if self.end_point_reached == "left": # if all positions upto left end point have been searched:
            right = position + distance # right of position is position plus distance
            if self.ordered_agents[right] == 0: # if the right position is empty
                return right # return right position
            else: # if right position is not empty
                distance += 1
                return self.search_neighborhood(position, distance) # look further

        elif self.end_point_reached == "right": # if all positions upto right end point have been searched:
            left = position - distance # left of position is position minus distance
            if self.ordered_agents[left] == 0: # if left position is empty
                return left # return left position
            else: # if left position is not empty
                distance += 1
                return self.search_neighborhood(position, distance) # look further
                    
        elif self.end_point_reached == None: # if positions upto neither extreme point have been searched
            right = position + distance # right is one plus the position
            left = position - distance # left is one minus the position
            
            if left == 0: # if left is zero
                self.end_point_reached = "left" # mark that all points upto left have been searched
        
            if right == self.highest_position: # if right is equals  the furthest position on right
                self.end_point_reached = "right" # mark all points to right have been searched


            if self.ordered_agents[right] != 0 and self.ordered_agents[left] != 0: # if positions on right and left are not empty
                distance += 1
                return self.search_neighborhood(position, distance) # search further
            elif self.ordered_agents[right] != 0 and self.ordered_agents[left] == 0: # if position on right is not empty, but left is empty
                return left
            elif self.ordered_agents[right] == 0 and self.ordered_agents[left] != 0: # if position on right is empty, but left is not empty
                return right
            elif self.ordered_agents[right] == 0 and self.ordered_agents[left] == 0: # if both positions are emtpy
                return random.choice([right, left]) # return one at random


    def find_place(self, position): # find an empty position on the line closest to the "position" argument
        if self.ordered_agents[position] == 0: # if the position on the ordered list of agents is empty, return the position.
            return position
        else: # if the position on the ordered list of agents is not empty, search for a place in the neighborhood of the "position" argument
            self.end_point_reached = None
            return self.search_neighborhood(position, 1)

    def place_agents(self): # place the agents at positions on the line
        for agent in self.agents: # for each agent in the list of agents
            number = agent.return_number() # ask agent to report its number
            place = self.find_place(number) # find an empty place on the line for the agent closest to its reported number
            self.ordered_agents[place] = agent # place the agent on the position
        self.agents = self.ordered_agents # change the list of agents with the ordered list of agents that places all agents in an order

