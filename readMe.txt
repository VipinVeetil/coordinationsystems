This is the model code for the paper titled “Coordination in centralized and decentralized systems”. The paper is published in The International Journal of Microsimulation. A copy of the paper is available on Social Science Research Network: https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2600735


The model code can be run from simulations.py
This will produce some csv files with data
The data can be plotted using analysis.py file

The model is written in Python 2.6
Please feel free to write to me with comments and questions at vipin.veetil AT gmail.com

Also, please feel free to use any or all of the code without citation.
