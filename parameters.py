# Coordination in Centralized and Decentralized Systems
# This file contains the parameters of the model
# Language: Python

number_of_agents = 100 # number of agents in the systems
probability_of_error = 0 # probability that an agent will make an error while reporting its number



decentralized_system_time_steps = 1000
compute_decentralized_coordination_over_time = False # whether to measure the coordination in the decentralized system at every time step

iterations_error = 10000
iterations_agents = 1000
cores = 6 # maximum number of cores of the machine to use for simulations


