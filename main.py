
from __future__ import division

import decentralized_system
import centralized_system
import parameters as p

class Main(object):
    def __init__(self):
        self.centralized_coordination = []
        self.decentralized_coordination = []
        self.decentralized_coordination_time_steps = []
        self.iterations = 0
    
    def decentralized_system_simulation(self):
        decentralized_instance = decentralized_system.Decentralized_System()
        decentralized_instance.create_agents()
        decentralized_instance.assign_attributes()
        decentralized_instance.process()
        decentralized_instance.compute_coordination()
        self.decentralized_coordination.append(decentralized_instance.coordination)
        if p.compute_decentralized_coordination_over_time == True:
            self.decentralized_coordination_time_steps.append(decentralized_instance.coordination_time_series)
    
    def centralized_system_simulation(self):
        centralized_instance = centralized_system.Centralized_System()
        centralized_instance.create_agents()
        centralized_instance.assign_attributes()
        centralized_instance.place_agents()
        centralized_instance.compute_coordination()
        self.centralized_coordination.append(centralized_instance.coordination)
    
    def centralized_iterations(self):
        for iteration in xrange(self.iterations):
            self.centralized_system_simulation()

    def decentralized_iterations(self):
        for iteration in xrange(self.iterations):
            self.decentralized_system_simulation()

"""
main_instance = Main()
p.number_of_agents = 100
p.probability_of_error = 0.1
main_instance.centralized_system_simulation()
print main_instance.centralized_coordination
"""
